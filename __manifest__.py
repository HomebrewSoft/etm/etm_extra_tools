# -*- coding: utf-8 -*-
{
    'name': 'Extra tools for ETM',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'sale',
    ],
    'data': [
        'views/res_config_settings.xml',
    ],
    'auto_install': True,
}
