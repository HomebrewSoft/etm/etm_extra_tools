# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductsOnPricelist(models.Model):
    _inherit = "sale.order.line"

    @api.onchange("product_id")
    def onchange_product_id(self):
        if not self.order_id.pricelist_id:
            return
        items = self.order_id.pricelist_id.item_ids
        if "3_global" in items.mapped("applied_on"):
            return
        return {"domain": {"product_id": [("product_tmpl_id", "in", items.product_tmpl_id.ids)]}}
