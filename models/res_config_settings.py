# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class PartnerCodeInstallableSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    module_partner_code_generation = fields.Boolean(
        string='Partner Code Generator module',
    )
    module_product_code_generation = fields.Boolean(
        string='Product Code Generator module',
    )
